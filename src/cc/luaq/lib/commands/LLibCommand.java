package cc.luaq.lib.commands;

import cc.luaq.lib.command.LCommand;
import cc.luaq.lib.util.JSONMessage;
import cc.luaq.lib.util.LPlayer;
import cc.luaq.lib.util.LUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.plugin.Plugin;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * LLib v2 was created and distributed by Luaq.
 * Please do not redistribute as one's own regardless of the
 * situation, this project (code and all), belongs to Luaq.
 */
public class LLibCommand implements LCommand {

    @Override
    public List<List<String>> getCompletions() {
        return Arrays.asList(Arrays.asList("info", "version", "plugins", "reload"));
    }

    @Override
    public void onCommand(LPlayer sender, Command command, String[] args) {
        sender.send();
        boolean showWatermark = true;
        if (args.length >= 1){
            switch (args[0]) {
                case "info":
                    sender.send("&eThis server was developed with the help of &dLLib&e.");
                    break;
                case "version":
                    sender.send("&eThe current version of LLib is &dv" + Objects.requireNonNull(LUtils.getPlugin("LLib"), "Nani?").getDescription().getVersion() + "&e.");
                    break;
                case "plugins":
                    showWatermark = false;
                    sender.send("&eLLib is currently supporting:");
                    List<Plugin> pluginList = LUtils.getSupportingPlugins();
                    pluginList.forEach(plugin -> sender.send(new JSONMessage(" &e" + plugin.getName() + " ").appendString("&d[RELOAD]", "run_command", "/llib reload " + plugin.getName(), "&eClick to reload.")));
                    if (pluginList.isEmpty())
                        sender.send("&cNo plugins at the moment.");
                    break;
                case "reload":
                    if (args.length >= 2) {
                        List<Plugin> plugins = Arrays.stream(Bukkit.getPluginManager().getPlugins()).collect(Collectors.toList());
                        showWatermark = false;
                        Plugin plugin = LUtils.getPlugin(args[1]);
                        if (plugin != null) {
                            if (plugins.contains(plugin)) {
                                sender.send("&eReloading the plugin &d\"" + plugin.getName() + ".\"");
                                Bukkit.getPluginManager().disablePlugin(plugin);
                                Bukkit.getPluginManager().enablePlugin(plugin);
                                sender.send("&eReloaded the plugin &d\"" + plugin.getName() + ".\"");
                            } else {
                                sender.send("&cThis plugin does not depend on LLib and cannot be refreshed.");
                            }
                        } else {
                            sender.send("&cThis plugin is non-existent.");
                        }
                    }
                    break;
            }
        }
        if (showWatermark){
            sender.send("&eLLib is developed and maintained by &dLuaq&e.");
            sender.send(new JSONMessage("&eCheck it out here: ").appendString("&b&nhttp://lib.luaq.cc/", "open_url", "http://lib.luaq.cc/", "&eClick to open."));
        }
        sender.send();
    }

}
