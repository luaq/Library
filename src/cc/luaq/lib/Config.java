package cc.luaq.lib;

import cc.luaq.lib.config.util.ConfigClass;
import cc.luaq.lib.config.util.ConfigOption;

/**
 * LLib v2 was created and distributed by Luaq.
 * Please do not redistribute as one's own regardless of the
 * situation, this project (code and all), belongs to Luaq.
 */
@ConfigClass(jsonFile = "llib.json", parentPlugin = LLib.class)
public class Config {

    /**
     * This option is recommended to be enabled if your server version is not found.
     */
    @ConfigOption(optionPath = "use_reflection")
    public static final boolean USE_REFLECTION = false;

    @ConfigOption(optionPath = "command_console_use_disabled")
    public static final String CONSOLE_DISABLED = "&cSorry, but this command is disabled and cannot be used in console.";

    @ConfigOption(optionPath = "command_usage_default")
    public static final String USAGE_MESSAGE = "&cUsage: $::command_usage";

    @ConfigOption(optionPath = "command_error_message")
    public static final String ERROR_MESSAGE = "&cSorry, but this command is currently broken and has an unhandled exception.";

    @ConfigOption(optionPath = "show_command_error_log")
    public static final boolean DISPLAY_ERROR_LOG = true;

}
