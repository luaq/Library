package cc.luaq.lib.gui.events;

import cc.luaq.lib.gui.MenuGUI;
import cc.luaq.lib.gui.MenuItem;
import cc.luaq.lib.gui.handlers.MenuClick;
import cc.luaq.lib.gui.handlers.MenuHandler;
import cc.luaq.lib.util.LPlayer;
import cc.luaq.lib.util.LUtils;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;

import java.util.List;
import java.util.stream.Collectors;

/**
 * LLib v2 was created and distributed by Luaq.
 * Please do not redistribute as one's own regardless of the
 * situation, this project (code and all), belongs to Luaq.
 */
public class MenuListener implements Listener {

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event){
        MenuGUI gui = null;
        MenuItem item = null;
        List<MenuClick> clickHandlers = null;
        int slot = event.getSlot();

        try {
            for (MenuGUI menuGUI : MenuGUI.getMenuGUIs()) {
                if (LUtils.color(menuGUI.getTitle()).equals(event.getClickedInventory().getTitle())){
                    MenuItem menuItem = menuGUI.getItemAt(event.getSlot());
                    if (menuItem != null){
                        List<MenuHandler> clickEvents = menuItem.getClickHandlers();
                        clickHandlers = clickEvents.stream().filter(handler -> handler instanceof MenuClick).map(menuHandler -> (MenuClick)menuHandler).collect(Collectors.toList());
                        item = menuItem;
                    }
                    gui = menuGUI;
                    break;
                }
            }
        } catch (NullPointerException ex){
            return;
        }

        if (gui != null && item != null)
            event.setCancelled(true);

        if (clickHandlers != null && !clickHandlers.isEmpty()){
            for (MenuClick handler : clickHandlers)
                handler.onClick(new LPlayer(event.getWhoClicked()), gui, item, slot, event.getClick());
        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event){
        Inventory inventory = event.getInventory();
        MenuGUI gui = null;

        for (MenuGUI menuGUI : MenuGUI.getMenuGUIs()) {
            if (LUtils.color(menuGUI.getTitle()).equals(inventory.getTitle())){
                gui = menuGUI;
                break;
            }
        }

        if (gui != null && gui.doDisposeOnClose())
            gui.dispose();
    }

}
