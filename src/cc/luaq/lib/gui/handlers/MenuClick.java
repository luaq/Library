package cc.luaq.lib.gui.handlers;

import cc.luaq.lib.gui.MenuGUI;
import cc.luaq.lib.gui.MenuItem;
import cc.luaq.lib.util.LPlayer;
import org.bukkit.event.inventory.ClickType;

/**
 * LLib v2 was created and distributed by Luaq.
 * Please do not redistribute as one's own regardless of the
 * situation, this project (code and all), belongs to Luaq.
 */
public interface MenuClick extends MenuHandler {

    /**
     * @param clicker The player that clicked on the item and triggered the event.
     * @param clickedGUI The GUI that the click occurred in.
     * @param itemClicked The item that was clicked to trigger this method.
     * @param slotClicked The slot that the item is located in.
     * @param type The type of click that occurred.
     */
    void onClick(LPlayer clicker, MenuGUI clickedGUI, MenuItem itemClicked, int slotClicked, ClickType type);

}
