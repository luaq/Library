package cc.luaq.lib.gui;

import cc.luaq.lib.util.LPlayer;
import cc.luaq.lib.util.LUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * LLib v2 was created and distributed by Luaq.
 * Please do not redistribute as one's own regardless of the
 * situation, this project (code and all), belongs to Luaq.
 */
public class MenuGUI {

    private final static List<MenuGUI> MENU_GUIS = new ArrayList<>();
    private final boolean isShared;
    private final List<MenuItem> itemList = new ArrayList<>();
    private int size;
    private String title;
    private Inventory inventory;
    private boolean doDisposeOnClose = false;

    /**
     * @param title The title of the inventory that opens to the player.
     * @param size The size of the inventory that opens to the player (multiples of nine).
     */
    public MenuGUI(String title, int size) {
        this(title, size, false);
    }

    /**
     * @param title The title of the inventory that opens to the player.
     * @param size The size of the inventory that opens to the player (multiples of nine).
     * @param isShared Whether or not the inventory changes should be shared or per person.
     */
    public MenuGUI(String title, int size, boolean isShared) {
        this.title = title;
        this.isShared = isShared;
        this.inventory = Bukkit.createInventory(null, size, LUtils.color(title));

        setSize(size);

        MENU_GUIS.add(this);
    }

    /**
     * @return A list of the created MenuGUIs.
     */
    public static List<MenuGUI> getMenuGUIs() {
        return MENU_GUIS;
    }

    /**
     * @return An inventory with all the items set in place.
     * @throws IllegalStateException Thrown when a GUI is attempted to be opened while {@link #doDisposeOnClose}
     */
    public Inventory generateInventory() throws IllegalStateException{
        if (doDisposeOnClose)
            throw new IllegalStateException("The menu is currently in a state that cannot open for a player.");

        Inventory inventory = (isShared) ? this.inventory : Bukkit.createInventory(null, size, LUtils.color(title));
        itemList.forEach(menuItem -> inventory.setItem(menuItem.getInventorySlot(), menuItem.generateItem()));

        return inventory;
    }

    /**
     * @param player The player that the GUI should open to.
     */
    public void displayGUI(Player player) {
        if (player != null)
            player.openInventory(generateInventory());
    }

    /**
     * @param player The player that the GUI should open to.
     */
    public void displayGUI(LPlayer player) {
        if (player != null && player.isPlayer())
            displayGUI(player.getPlayer());
    }

    /**
     * @param items The menu items that you want added to the menu.
     */
    public void addItems(MenuItem... items) {
        itemList.addAll(Arrays.asList(items));
    }

    /**
     * @param inventorySlot The slot that the item is at.
     * @return A MenuItem object.
     */
    public MenuItem getItemAt(int inventorySlot) {
        return itemList.stream().filter(item -> item.getInventorySlot() == inventorySlot).findFirst().orElse(null);
    }

    /**
     * Clears all the items in the menu.
     */
    public void clearItems() {
        itemList.clear();
    }

    /**
     * @return A list of the items in the menu.
     */
    public List<MenuItem> getItemList() {
        return itemList;
    }

    public int getSize() {
        return size;
    }

    /**
     * @param size The size of the inventory menu.
     * @throws IllegalArgumentException The MenuGUI size must be in-between 9 and 54 and be a multiple of 9
     */
    public void setSize(int size) throws IllegalArgumentException {
        if ((size < 9 || size > 54) && size % 9 != 0)
            throw new IllegalArgumentException("The MenuGUI size must be in-between 9 and 54 and be a multiple of 9");
        this.size = size;
    }

    /**
     * @return The title of the inventory.
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title of the inventory.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Removes the GUI from being registered in {@link #MENU_GUIS} when it's closed.
     */
    public void setDisposeOnClose(){
        doDisposeOnClose = true;
    }

    /**
     * @return Whether or not the GUI will be disposed of when closed.
     */
    public boolean doDisposeOnClose() {
        return doDisposeOnClose;
    }

    /**
     * Disposes of the GUI and renders it useless.
     */
    public void dispose(){
        MENU_GUIS.remove(this);

        doDisposeOnClose = true;
        if (this.isShared)
            this.inventory = null;
    }

}
