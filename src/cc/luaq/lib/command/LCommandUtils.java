package cc.luaq.lib.command;

import cc.luaq.lib.Config;
import cc.luaq.lib.util.LPlayer;
import cc.luaq.lib.util.LUtils;
import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

/**
 * LLib v2 was created and distributed by Luaq.
 * Please do not redistribute as one's own regardless of the
 * situation, this project (code and all), belongs to Luaq.
 */
public class LCommandUtils {

    private static List<String> registeredCommands = new ArrayList<>();

    /**
     * @param plugin The plugin associated with the commandName.
     * @param commandName The command name in the plugin.yml.
     * @param executor The executor class.
     */
    public static void register(JavaPlugin plugin, String commandName, LCommand executor){
        register(plugin, commandName, executor, false);
    }

    /**
     * @param plugin The plugin associated with the commandName.
     * @param commandName The command name in the plugin.yml.
     * @param executor The executor class.
     * @param disableConsole Whether or not the console will be able to use this command.
     */
    public static void register(JavaPlugin plugin, String commandName, LCommand executor, boolean disableConsole){
        PluginCommand command = plugin.getCommand(commandName);
        command.setExecutor((commandSender, cmd, s, strings) -> {
            LPlayer sender = new LPlayer(commandSender);
            try {
                if(!sender.isPlayer() && !disableConsole || sender.isPlayer()){
                    executor.onCommand(sender, cmd, strings);
                } else sender.send(Config.CONSOLE_DISABLED);
            } catch (Exception ex){
                final String consoleMessage1 = LUtils.color("&cThis was caused by " + ex.toString() + ".");
                final String consoleMessage2 = LUtils.color("&cError occurred at " + LUtils.getExceptionDetails(ex) + ".");
                sender.send();
                sender.send(Config.ERROR_MESSAGE);
                if (Config.DISPLAY_ERROR_LOG){
                    sender.send();
                    sender.send(consoleMessage1);
                    sender.send(consoleMessage2);
                }
                sender.send();

                System.out.println(" ");
                System.out.println("An error occurred while " + sender.getName() + " was trying to execute the command /" + commandName + ".");
                System.out.println(" ");
                System.out.println(consoleMessage1);
                System.out.println(consoleMessage2);
                System.out.println(" ");
            }
            return true;
        });
        command.setTabCompleter((commandSender, cmd, s, strings) -> executor.onTab(new LPlayer(commandSender), cmd, strings));
        registeredCommands.add(commandName);
    }

    /**
     * @return The commands registered through LLib.
     */
    public static List<String> getRegisteredCommands() {
        return registeredCommands;
    }

}
