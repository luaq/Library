package cc.luaq.lib.command;

import cc.luaq.lib.util.LPlayer;
import cc.luaq.lib.util.LUtils;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * LLib v2 was created and distributed by Luaq.
 * Please do not redistribute as one's own regardless of the
 * situation, this project (code and all), belongs to Luaq.
 */
public interface LCommand {

    /**
     * @param sender Who executed the command.
     * @param command The command that was executed.
     * @param args The arguments that were sent along with executing the command.
     */
    void onCommand(LPlayer sender, Command command, String args[]);

    /**
     * @return The command's completions sorted by List<args>
     */
    default List<List<String>> getCompletions() {
        return new ArrayList<>();
    }

    /**
     * @param player The player that requested the tab completion.
     * @param command The command that is being executed.
     * @param args The arguments that are being completed.
     * @return A list of AutoCompletions.
     */
    default List<String> onTab(LPlayer player, Command command, String args[]){
        List<List<String>> completions = getCompletions();
        List<String> playerList = LUtils.getPlayers().stream().map(Player::getName).sorted(String.CASE_INSENSITIVE_ORDER).collect(Collectors.toList());
        if (completions.isEmpty() || args.length > completions.size())
            return playerList;

        List<String> strings;
        try {
            strings = completions.get(args.length - 1).stream().filter(s -> s.toLowerCase().startsWith(args[args.length - 1].toLowerCase())).sorted(String.CASE_INSENSITIVE_ORDER).collect(Collectors.toList());

            /* means it's a keyword */
            if (strings.size() == 1){
                String s = strings.get(0);
                if (s.equals("%online_players%"))
                    return playerList;
                if (s.equals("%opped_players%"))
                    return LUtils.getPlayers().stream().filter(Player::isOp).map(Player::getName).sorted(String.CASE_INSENSITIVE_ORDER).collect(Collectors.toList());
            }
        } catch (IndexOutOfBoundsException ex) {
            return playerList;
        }

        return strings;
    }

}
