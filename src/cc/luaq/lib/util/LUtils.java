package cc.luaq.lib.util;

import cc.luaq.lib.Config;
import cc.luaq.lib.LLib;
import cc.luaq.lib.util.nms.LNMS;
import cc.luaq.lib.util.nms.versions.*;
import com.google.gson.JsonObject;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * LLib v2 was created and distributed by Luaq.
 * Please do not redistribute as one's own regardless of the
 * situation, this project (code and all), belongs to Luaq.
 */
public class LUtils {

    public static final String NMS_VERSION;
    private static LNMS nmsClass;

    static {
        String version;
        try {
            version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
        } catch (ArrayIndexOutOfBoundsException whatKindOfVersionIsThis){
            version = null;
            whatKindOfVersionIsThis.printStackTrace();
        }
        NMS_VERSION = version;
    }

    public static boolean refreshNMS(){
        switch(NMS_VERSION){
            case "v1_8_R3":
                nmsClass = new LUtils_v1_8_R3();
                break;
            case "v1_9_R1":
                nmsClass = new LUtils_v1_9_R1();
                break;
            case "v1_9_R2":
                nmsClass = new LUtils_v1_9_R2();
                break;
            case "v1_10_R1":
                nmsClass = new LUtils_v1_10_R1();
                break;
            case "v1_11_R1":
                nmsClass = new LUtils_v1_11_R1();
                break;
            case "v1_12_R1":
                nmsClass = new LUtils_v1_12_R1();
                break;
            case "v1_13_R1":
                nmsClass = new LUtils_v1_13_R1();
                break;
            case "v1_13_R2":
                nmsClass = new LUtils_v1_13_R2();
                break;
            default:
                nmsClass = null;
                return false;
        }

        return true;
    }

    public static LNMS getNmsClass() {
        return nmsClass;
    }

    /**
     * @param text The text.
     * @param clickAction (null | "open_url" | "run_command" | "suggest_command" | "suggest_text") The action that occurs when the text is clicked.
     * @param clickValue The value that is checked when the text is clicked.
     * @param hoverText The text that appears when the line is hovered.
     * @return The JSON string.
     */
    public static String generateJSON(String text, String clickAction, String clickValue, String hoverText){
        JsonObject object = new JsonObject();
        object.addProperty("text", text);

        if (clickAction != null && clickValue != null){
            JsonObject clickEvent = new JsonObject();
            clickEvent.addProperty("action", clickAction);
            clickEvent.addProperty("value", clickValue);

            object.add("clickEvent", clickEvent);
        }
        if (hoverText != null){
            JsonObject hoverEvent = new JsonObject();
            hoverEvent.addProperty("action", "show_text");
            hoverEvent.addProperty("value", hoverText);

            object.add("hoverEvent", hoverEvent);
        }

        return object.toString();
    }

    /**
     * @param exception The exception.
     * @return class(methodName:lineNumber)
     */
    public static String getExceptionDetails(Exception exception) {
        StackTraceElement[] stackTraceElement = exception.getStackTrace();

        String className = "";
        String methodName = "";
        int lineNumber = 0;

        try {
            String packageName = LLib.class.getPackage().getName();
            for (StackTraceElement aStackTraceElement : stackTraceElement) {
                if (aStackTraceElement.getClassName().startsWith(packageName)) {
                    String name = aStackTraceElement.getFileName();
                    className = name.substring(0, name.indexOf(".java"));
                    methodName = aStackTraceElement.getMethodName();
                    lineNumber = aStackTraceElement.getLineNumber();
                    break;
                }
            }
        } catch (Exception e2) {
        }

        return String.format("%s(%s:%s)", className, methodName, String.valueOf(lineNumber));
    }

    /**
     * @param player Player that you want to send the action bar message to.
     * @param message The message that will be sent to the player's action bar.
     * @return Whether or not the method sent the message successfully.
     */
    public static boolean sendActionBar(Player player, String message) {
        if (NMS_VERSION != null) {
            if (Config.USE_REFLECTION) {
                try {
                    Class<?> chatSerializer = Class.forName("net.minecraft.server." + NMS_VERSION + ".IChatBaseComponent.ChatSerializer");
                    Object cbc = chatSerializer.getMethod("a", String.class).invoke(null, "{\"text\":\"" + color(message) + "\"}");
                    Class<?> craftPlayer = Class.forName("org.bukkit.craftbukkit." + NMS_VERSION + ".entity.CraftPlayer");
                    Object playerConnection = Class.forName("net.minecraft.server." + NMS_VERSION + ".EntityPlayer").getField("playerConnection").get(craftPlayer.getMethod("getHandle").invoke(player));
                    playerConnection.getClass().getMethod("sendPacket", cbc.getClass()).invoke(playerConnection, cbc);
                    return true;
                } catch (ClassNotFoundException | IllegalAccessException | NoSuchMethodException | InvocationTargetException | NoSuchFieldException e) {
                    e.printStackTrace();
                    return false;
                }
            } else if (nmsClass != null) {
                nmsClass.sendActionBar(player, message);
            }
        }
        return false;
    }

    /**
     * @param joinStr The string to join all others with.
     * @param strings The list of strings that you want joined.
     * @return The string that was created.
     */
    public static String join(String joinStr, List<String> strings){
        return join(joinStr, strings, 0, strings.size());
    }

    /**
     * @param joinStr The string to join all others with.
     * @param strings The list of strings that you want joined.
     * @param startPoint The starting point in the strings list.
     * @return The string that was created.
     */
    public static String join(String joinStr, List<String> strings, int startPoint){
        return join(joinStr, strings, startPoint, strings.size());
    }

    /**
     * @param joinStr The string to join all others with.
     * @param strings The list of strings that you want joined.
     * @param startPoint The starting point in the strings list.
     * @param endPoint The ending point in the strings list.
     * @return The string that was created.
     */
    public static String join(String joinStr, List<String> strings, int startPoint, int endPoint){
        StringBuilder builder = new StringBuilder();

        for(int i = startPoint; i < endPoint; i++){
            builder.append(strings.get(i));
            if(i != endPoint-1){
                builder.append(joinStr);
            }
        }

        return builder.toString();
    }

    /**
     * @param message The message that will send.
     */
    public static void sendAll(JSONMessage message){
        for(Player player : Bukkit.getOnlinePlayers()) nmsClass.sendMessage(player, message);
    }

    /**
     * @param perm The permission required to see the message.
     * @param message The message that will send.
     */
    public static void sendAll(String perm, JSONMessage message){
        for(Player player : Bukkit.getOnlinePlayers()) {
            if (player.hasPermission(perm)) nmsClass.sendMessage(player, message);
        }
    }

    /**
     * @param string The message that will send.
     */
    public static void sendAll(String string){
        for(Player player : Bukkit.getOnlinePlayers()) player.sendMessage(color(string));
    }

    /**
     * @param perm The permission required to see the message.
     * @param string The message that will send.
     */
    public static void sendAll(String perm, String string){
        for(Player player : Bukkit.getOnlinePlayers()) {
            if(player.hasPermission(perm)) player.sendMessage(color(string));
        }
    }

    /**
     * @param sound The sound that will be played.
     */
    public static void sendAll(Sound sound){
        for(Player player : Bukkit.getOnlinePlayers()) player.playSound(player.getLocation(), sound, 1.0F, 1.0F);
    }

    /**
     * @param perm The permission required to hear the sound.
     * @param sound The sound that will be played.
     */
    public static void sendAll(String perm, Sound sound){
        for(Player player : Bukkit.getOnlinePlayers()) {
            if(player.hasPermission(perm)) player.playSound(player.getLocation(), sound, 1.0F, 1.0F);
        }
    }

    /**
     * @param string The string that you want colored.
     * @return The colored string.
     */
    public static String color(String string) {
        return ChatColor.translateAlternateColorCodes('&', string);
    }

    /**
     * @return All the players that are online.
     */
    public static List<Player> getPlayers() {
        return new ArrayList<>(Bukkit.getOnlinePlayers());
    }

    /**
     * @param name The name you wish to get the offline player by.
     * @return Returns the OfflinePlayer object.
     */
    @SuppressWarnings("deprecation")
    public static OfflinePlayer getOfflinePlayer(String name){
        return Bukkit.getOfflinePlayer(name);
    }

    /**
     * @param uuid The uuid you wish to get the offline player by.
     * @return Returns the OfflinePlayer object.
     */
    public static OfflinePlayer getOfflinePlayer(UUID uuid){
        return Bukkit.getOfflinePlayer(uuid);
    }

    /**
     * @param name The name you wish to get the player by.
     * @return Returns the Player object (or null).
     */
    public static Player getPlayer(String name){
        return Bukkit.getOnlinePlayers().stream().filter(player -> player.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
    }

    /**
     * @param uuid The uuid you wish to get the player by.
     * @return Returns the Player object (or null).
     */
    public static Player getPlayer(UUID uuid){
        return Bukkit.getOnlinePlayers().stream().filter(player -> player.getUniqueId().equals(uuid)).findFirst().orElse(null);
    }

    /**
     * @param string The string to format with.
     * @param strings The strings to format with.
     * @return The formatted string.
     */
    public static String format(String string, String... strings){
        for (String s : strings) {
            String[] entry = s.split("=");
            if (entry.length > 1){
                if (string.contains("$::" + entry[0]))
                    string = string.replace("$::" + entry[0], entry[1]);
            } else {
                throw new IllegalArgumentException("You cannot set a variable value without defining it.");
            }
        }
        return string;
    }

    /**
     * @return A list of the plugins that are depending on LLib.
     */
    public static List<Plugin> getSupportingPlugins(){
        return Arrays.stream(Bukkit.getPluginManager().getPlugins()).filter(plugin -> plugin.getDescription().getDepend().contains("LLib")).collect(Collectors.toList());
    }

    /**
     * @param pluginName The name of the plugin.
     * @return The plugin object.
     */
    public static Plugin getPlugin(String pluginName){
        for (Plugin plugin : Bukkit.getPluginManager().getPlugins()) {
            if (plugin.getName().equalsIgnoreCase(pluginName))
                return plugin;
        }
        return null;
    }

    /**
     * @param pluginClass The class of the plugin.
     * @return The plugin.
     */
    public static Plugin getPlugin(Class<? extends Plugin> pluginClass){
        for (Plugin plugin : Bukkit.getPluginManager().getPlugins()) {
            if (plugin.getClass().equals(pluginClass))
                return plugin;
        }
        return null;
    }

    /**
     * @param string The string that you want to be broken up.
     * @return It returns the string that has been broken into multiple lines.
     */
    public static List<String> multiLine(String string){
        final List<String> strings = new ArrayList<>();
        final String[] words = string.split(" ");

        int w = 0;
        StringBuilder str = new StringBuilder();
        for(String word : words){
            if(w != 0 && w % 5 == 0){
                w = 0;
                strings.add(str.toString());
                str = new StringBuilder();
            }
            str.append(word).append(" ");
            if(word.length() > 2) {
                w++;
            }
        }

        return strings;
    }

}
