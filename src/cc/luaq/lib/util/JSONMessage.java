package cc.luaq.lib.util;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * LLib v2 was created and distributed by Luaq.
 * Please do not redistribute as one's own regardless of the
 * situation, this project (code and all), belongs to Luaq.
 */
public class JSONMessage {

    private final List<String> jsonComponents = new ArrayList<>();

    public JSONMessage(){
        this("");
    }

    /**
     * @param message The base message.
     */
    public JSONMessage(String message){
        this(message, null, null, null);
    }

    public JSONMessage(String message, String clickAction, String clickValue, String hoverText){
        appendString(message, clickAction, clickValue, hoverText);
    }

    /**
     * @param json The raw json that you want to append.
     * @return Updated JSONMessage class.
     */
    public JSONMessage appendRawJson(String json){
        jsonComponents.add(json);
        return this;
    }

    /**
     * Appends &r\n
     * @return Updated JSONMessage class.
     */
    public JSONMessage applyBreak() {
        appendString("&r\n");
        return this;
    }

    /**
     * @param message The string that you want to append to the message.
     * @return Updated JSONMessage class.
     */
    public JSONMessage appendString(String message) {
        appendString(message, null, null, null);
        return this;
    }

    /**
     * @param message The string that you want to append to the message.
     * @param clickAction (null | "open_url" | "run_command" | "suggest_command" | "suggest_text") The action that occurs when the text is clicked.
     * @param clickValue The value that is checked when the text is clicked.
     * @param hoverText The text that appears when the line is hovered.
     * @return Updated JSONMessage class.
     */
    public JSONMessage appendString(String message, String clickAction, String clickValue, String hoverText) {
        appendRawJson(LUtils.generateJSON(message, clickAction, clickValue, hoverText));
        return this;
    }

    /**
     * @return Complete JSON message.
     */
    public String asJson(){
        return String.format("[\"\", %s]", LUtils.color(jsonComponents.stream().collect(Collectors.joining(","))));
    }

    /**
     * @return The json components that are going into the message.
     */
    public List<String> getJsonComponents() {
        return jsonComponents;
    }

}

