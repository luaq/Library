package cc.luaq.lib.util;

import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftMetaBook;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * LLib v2 was created and distributed by Luaq.
 * Please do not redistribute as one's own regardless of the
 * situation, this project (code and all), belongs to Luaq.
 */
public class Book {

    private String bookName;
    private String author;

    private final List<String> lines;
    private final List<String> pages;

    /**
     * @param bookName The display name of the book item.
     * @param author The author of the book item.
     */
    public Book(String bookName, String author) {
        this.bookName = bookName;
        this.author = author;

        this.lines = new ArrayList<>();
        this.pages = new ArrayList<>();
    }

    /**
     * @param author The new author of the book item.
     * @return Updated Book class.
     */
    public Book setAuthor(String author) {
        this.author = author;
        return this;
    }

    /**
     * @param bookName The new display name of the book item.
     * @return Updated Book class.
     */
    public Book setBookName(String bookName) {
        this.bookName = bookName;
        return this;
    }

    /**
     * @return Updated Book class.
     */
    public Book newPage(){
        pages.add(joinJsonList(lines));
        lines.clear();

        return this;
    }

    /**
     * @return Updated Book class.
     */
    public Book resetLines(){
        lines.clear();
        return this;
    }

    /**
     * @param line The line that you want to be added to the current page.
     * @return Updated Book class.
     */
    public Book addLine(String line){
        addLine(line, null, null, null, true);
        return this;
    }

    /**
     * @param line The line that you want to be added to the current page.
     * @param clickAction (null | "open_url" | "run_command") The action that occurs when the line is clicked.
     * @param clickValue The value that is checked when the text is clicked.
     * @param hoverText The text that appears when the line is hovered.
     * @return Updated Book class.
     */
    public Book addLine(String line, String clickAction, String clickValue, String hoverText){
        addLine(line, clickAction, clickValue, hoverText, true);
        return this;
    }

    /**
     * @param line The line that you want to be added to the current page.
     * @param clickAction (null | "open_url" | "run_command") The action that occurs when the line is clicked.
     * @param clickValue The value that is checked when the text is clicked.
     * @param hoverText The text that appears when the line is hovered.
     * @param doBreak Whether or not it should append \n
     * @return Updated Book class.
     */
    public Book addLine(String line, String clickAction, String clickValue, String hoverText, boolean doBreak){
        addRawJson(LUtils.generateJSON(line + ((doBreak) ? "\n" : ""), clickAction, clickValue, hoverText));
        return this;
    }

    /**
     * @param json The raw json to be added.
     * @return Updated Book class.
     */
    public Book addRawJson(String json){
        lines.add(json);
        return this;
    }

    /**
     * @return The lines in the book.
     */
    public List<String> getLines() {
        return lines;
    }

    /**
     * @return The author of the book.
     */
    public String getAuthor() {
        return author;
    }

    /**
     * @return The display name of the book.
     */
    public String getBookName() {
        return bookName;
    }

    /**
     * @return The book ItemStack to be used.
     */
    public ItemStack build(){
        if (!lines.isEmpty()) {
            pages.add(joinJsonList(lines));
        }

        ItemStack stack = new ItemStack(Material.WRITTEN_BOOK, 1);
        BookMeta meta = (BookMeta) stack.getItemMeta();

        meta.setAuthor(author);
        meta.setTitle(bookName);
        meta.setDisplayName(bookName);

        for (String page : pages){
            CraftMetaBook metaBook = (CraftMetaBook) meta;
            try {
                metaBook.pages.add(IChatBaseComponent.ChatSerializer.a(LUtils.color(page)));
            } catch (Exception e) {
            }
        }

        stack.setItemMeta(meta);

        return stack;
    }

    private String joinJsonList(List<String> list){
        return String.format("[\"\",%s]", list.stream().collect(Collectors.joining(",")));
    }

}
