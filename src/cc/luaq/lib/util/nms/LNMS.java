package cc.luaq.lib.util.nms;

import cc.luaq.lib.util.JSONMessage;
import org.bukkit.entity.Player;

/**
 * LLib v2 was created and distributed by Luaq.
 * Please do not redistribute as one's own regardless of the
 * situation, this project (code and all), belongs to Luaq.
 */
public interface LNMS {

    void sendMessage(Player player, JSONMessage message);
    void sendActionBar(Player player, String message);
    void sendTabHF(Player player, String header, String footer);
    void sendTitle(Player player, String title, String subtitle, int fadeIn, int stay, int fadeOut);
    void openBook(Player player);

}
