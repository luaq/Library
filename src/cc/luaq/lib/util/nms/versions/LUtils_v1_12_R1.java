package cc.luaq.lib.util.nms.versions;

import cc.luaq.lib.util.JSONMessage;
import cc.luaq.lib.util.LUtils;
import cc.luaq.lib.util.nms.LNMS;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import net.minecraft.server.v1_12_R1.*;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;

@SuppressWarnings("Duplicates")
public class LUtils_v1_12_R1 implements LNMS {

    @Override
    public void sendMessage(Player player, JSONMessage message) {
        PacketPlayOutChat playOutChat = new PacketPlayOutChat(IChatBaseComponent.ChatSerializer.a(message.asJson()));
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(playOutChat);
    }

    @Override
    public void openBook(Player player) {
        ByteBuf byteBuf = Unpooled.buffer(256);
        byteBuf.setByte(0, (byte)0);
        byteBuf.writerIndex(1);

        PacketPlayOutCustomPayload payload = new PacketPlayOutCustomPayload("MC|BOpen", new PacketDataSerializer(byteBuf));
        ((CraftPlayer)player).getHandle().playerConnection.sendPacket(payload);
    }

    @Override
    public void sendActionBar(Player player, String message) {
        IChatBaseComponent cbc = IChatBaseComponent.ChatSerializer.a("{\"text\":\""+LUtils.color(message)+"\"}");
        PacketPlayOutChat ppac = new PacketPlayOutChat(cbc, ChatMessageType.GAME_INFO);
        ((CraftPlayer)player).getHandle().playerConnection.sendPacket(ppac);
    }

    @Override
    public void sendTabHF(Player player, String header, String footer) {
        CraftPlayer cp = (CraftPlayer) player;
        PlayerConnection connection = cp.getHandle().playerConnection;
        IChatBaseComponent headerJSON = IChatBaseComponent.ChatSerializer.a("{\"text\":\""+LUtils.color(header)+"\"}");
        IChatBaseComponent footerJSON = IChatBaseComponent.ChatSerializer.a("{\"text\":\""+LUtils.color(footer)+"\"}");
        PacketPlayOutPlayerListHeaderFooter packet = new PacketPlayOutPlayerListHeaderFooter();

        try{
            Field headerField = packet.getClass().getDeclaredField("a");
            headerField.setAccessible(true);
            headerField.set(packet, headerJSON);
            headerField.setAccessible(!headerField.isAccessible());

            Field footerField = packet.getClass().getDeclaredField("b");
            footerField.setAccessible(true);
            footerField.set(packet, footerJSON);
            footerField.setAccessible(!footerField.isAccessible());
        } catch (Exception e){
            e.printStackTrace();
        }

        connection.sendPacket(packet);
    }

    @Override
    public void sendTitle(Player player, String title, String subtitle, int fadeIn, int stay, int fadeOut) {
        CraftPlayer craftPlayer = (CraftPlayer) player;
        PlayerConnection connection = craftPlayer.getHandle().playerConnection;
        IChatBaseComponent titleJSON = IChatBaseComponent.ChatSerializer.a("{\"text\":\""+LUtils.color(title)+"\"}");
        IChatBaseComponent subtitleJSON = IChatBaseComponent.ChatSerializer.a("{\"text\":\""+LUtils.color(subtitle)+"\"}");
        PacketPlayOutTitle titlePacket = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, titleJSON, fadeIn, stay, fadeOut);
        PacketPlayOutTitle subtitlePacket = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, subtitleJSON, fadeIn, stay, fadeOut);
        connection.sendPacket(titlePacket);
        connection.sendPacket(subtitlePacket);
    }

}
