package cc.luaq.lib.util;

import cc.luaq.lib.Config;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Objects;
import java.util.UUID;

/**
 * LLib v2 was created and distributed by Luaq.
 * Please do not redistribute as one's own regardless of the
 * situation, this project (code and all), belongs to Luaq.
 */
public class LPlayer {

    private final CommandSender rawSender;
    private final Player player;
    private final boolean isPlayer;

    public LPlayer(Player player){
        this.rawSender = null;
        this.player = player;
        this.isPlayer = true;
    }

    public LPlayer(CommandSender sender){
        this.rawSender = sender;
        this.isPlayer = rawSender instanceof Player;
        this.player = (isPlayer) ? getPlayer() : null;
    }

    /**
     * @param message The message that you want sent.
     */
    final public void talk(String message){
        if (isPlayer)
            player.chat(LUtils.color(message));
        else
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "say " + LUtils.color(message));
    }

    /**
     * @param command The command that the usage will be sent from.
     */
    final public void sendUsage(Command command){
        String usage = command.getUsage();
        send(LUtils.format(Config.USAGE_MESSAGE, "command_usage=" + ((usage != null) ? usage : "UNSPECIFIED")));
    }

    /**
     * @param permissionNode The node that the sender should have.
     * @return Whether or not the sender has the permission node.
     */
    final public boolean hasPermission(String permissionNode){
        return this.rawSender.hasPermission(permissionNode);
    }

    /**
     * @return The unique id of the player that executed the command.
     */
    final public UUID getUniqueId(){
        return (isPlayer()) ? Objects.requireNonNull(getPlayer(), "This is pretty weird, but the player is non existent.").getUniqueId() : null;
    }

    /**
     * @return The name of the sender.
     */
    final public String getName(){
        return this.rawSender.getName();
    }

    /**
     * Sends an empty message.
     */
    final public void send(){
        send("");
    }

    /**
     * @param message The message to be sent to the player.
     */
    final public void send(String message){
        this.rawSender.sendMessage(LUtils.format(LUtils.color("&7" + message), "sender_name=" + getName()));
    }

    /**
     * @param message The message to be sent to the player.
     */
    final public void send(JSONMessage message){
        if (!isPlayer()){
            send(message.asJson());
            return;
        }
        LUtils.getNmsClass().sendMessage(player, message);
    }

    /**
     * @return Whether it was a player that ran the command or something else.
     */
    final public boolean isPlayer(){
        return isPlayer && player != null;
    }

    /**
     * @return The player that executed the command.
     */
    final public Player getPlayer(){
        return (this.isPlayer) ? ((this.player != null) ? this.player : (Player) this.rawSender) : null;
    }

    /**
     * @return The raw sender in-case there is a missing method.
     */
    final public CommandSender getRawSender() {
        return rawSender;
    }
}
