package cc.luaq.lib.config;

import cc.luaq.lib.config.util.ConfigClass;
import cc.luaq.lib.config.util.ConfigOption;
import cc.luaq.lib.util.LUtils;
import com.google.gson.*;
import com.google.gson.stream.JsonReader;
import org.bukkit.plugin.Plugin;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;

/**
 * LLib v2 was created and distributed by Luaq.
 * Please do not redistribute as one's own regardless of the
 * situation, this project (code and all), belongs to Luaq.
 */
public class LConfig {

    private final static Map<Class<?>, List<Field>> configClasses = new HashMap<>();

    /**
     * @param configClass The config class that you want to be registered.
     * @return Whether or not it was successfully registered.
     */
    public static boolean registerClass(Class<?> configClass) {
        if (!configClass.isAnnotationPresent(ConfigClass.class) || isRegistered(configClass))
            return false;

        List<Field> fields = new ArrayList<>();
        for (Field field : configClass.getDeclaredFields()) {
            if (field.isAnnotationPresent(ConfigOption.class) && Modifier.isStatic(field.getModifiers()) && Modifier.isPublic(field.getModifiers()))
                fields.add(field);
        }
        if (fields.isEmpty())
            return false;

        configClasses.put(configClass, fields);
        if (configExists(configClass)) {
            if (!updateConfigValues(configClass)) {
                System.out.println("Failed to update the values for the config class " + configClass.getName());
            }
        } else {
            if(!writeConfigFile(configClass)){
                configClasses.remove(configClass);
                return false;
            }
        }

        return configClasses.containsKey(configClass);
    }

    /*
     * credit:
     * https://stackoverflow.com/questions/2474017/using-reflection-to-change-static-final-file-separatorchar-for-unit-testing
     */
    private static void setFinalStatic(Field field, Object newValue) throws Exception {
        field.setAccessible(true);

        Field modifiersField = Field.class.getDeclaredField("modifiers");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

        field.set(null, newValue);
    }

    /**
     * @param configClass The config class.
     * @return Whether or not updating the config class values was successful.
     */
    public static boolean updateConfigValues(Class<?> configClass){
        if (isRegistered(configClass)) {
            File jsonFile = getConfigFile(configClass);
            if (configExists(configClass) && jsonFile != null) {
                JsonParser parser = new JsonParser();

                try {
                    BufferedReader bufferedReader = new BufferedReader(new FileReader(jsonFile));
                    StringBuilder json = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null)
                        json.append(line);
                    JsonReader reader = new JsonReader(new StringReader(json.toString()));
                    reader.setLenient(true);
                    JsonObject object = parser.parse(reader).getAsJsonObject();

                    Iterator<Map.Entry<String, JsonElement>> iterator = object.entrySet().iterator();
                    List<Field> classFields = getEntry(configClass).getValue();
                    List<Field> updatedFields = new ArrayList<>();
                    while (iterator.hasNext()){
                        Map.Entry<String, JsonElement> entry = iterator.next();
                        Field valueField = null;
                        for (Field field : classFields) {
                            if (field.getAnnotation(ConfigOption.class).optionPath().equals(entry.getKey())){
                                valueField = field;
                                break;
                            }
                        }
                        if (valueField != null){
                            try {
                                if (valueField.getType() == Map.class){
                                    Object o = valueField.get(null);
                                    @SuppressWarnings("unchecked")
                                    Map<String, Object> map = (o instanceof Map) ? (Map<String, Object>) o : null;
                                    if (map != null){
                                        Set<Map.Entry<String, JsonElement>> entries = entry.getValue().getAsJsonObject().entrySet();
                                        for (Map.Entry<String, JsonElement> mapEntry : entries)
                                            map.put(mapEntry.getKey(), convertElementToObject(mapEntry.getValue()));
                                    }
                                } else {
                                    setFinalStatic(valueField, new Gson().fromJson(entry.getValue(), valueField.getType()));
                                }
                                updatedFields.add(valueField);
                            } catch (Exception e) {
                                e.printStackTrace();
                                return false;
                            }
                        }
                    }
                    if (classFields.stream().anyMatch(field -> !updatedFields.contains(field)))
                        writeConfigFile(configClass);

                    return true;
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }
            }
        }
        return false;
    }

    /**
     * Acts as a "save config values."
     * @param configClass The config class.
     * @return Whether or not updating/creating the file was successful.
     */
    public static boolean writeConfigFile(Class<?> configClass){
        if (isRegistered(configClass)){
            Map.Entry<Class<?>, List<Field>> entry = getEntry(configClass);
            Map<String, Object> configValues = new HashMap<>();
            for (Field field : entry.getValue()) {
                ConfigOption option = field.getAnnotation(ConfigOption.class);
                try {
                    configValues.put(option.optionPath(), field.get(null));
                } catch (IllegalAccessException fieldShouldBeStaticException) {
                    fieldShouldBeStaticException.printStackTrace();
                    return false;
                }
            }
            Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
            JsonObject object = new JsonObject();
            configValues.forEach((path, value) -> object.add(path, convertObjectToElement(value)));

            String json = gson.toJson(object);
            try {
                File configFile = getConfigFile(configClass);
                if (configFile != null){
                    if (!configExists(configClass)){
                        if (!configFile.getParentFile().exists())
                            if(!configFile.getParentFile().mkdir())
                                return false;
                        if (!configFile.createNewFile())
                            return false;
                    }
                    FileWriter writer = new FileWriter(configFile);

                    writer.write(json);
                    writer.flush();
                    writer.close();

                    return true;
                } else {
                    return false;
                }
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    /*
    * I am so sorry at the level of thought that went into these methods.
    * convertElementToObject and convertObjectToElement are both terribly written.
     */
    private static Object convertElementToObject(JsonElement element){
        Object object = element;
        JsonPrimitive primitive = element.getAsJsonPrimitive();
        if (primitive.isString())
            object = primitive.getAsString();
        else if (primitive.isBoolean())
            object = primitive.getAsBoolean();
        else if (primitive.isNumber())
            object = primitive.getAsNumber();
        return object;
    }

    private static JsonElement convertObjectToElement(Object value){
        JsonElement element = null;
        if (value instanceof String)
            element = new JsonPrimitive((String) value);
        else if (value instanceof Boolean)
            element = new JsonPrimitive((boolean) value);
        else if (value instanceof Integer)
            element = new JsonPrimitive((int) value);
        else if (value instanceof Double)
            element = new JsonPrimitive((double) value);
        else if (value instanceof Float)
            element = new JsonPrimitive((float) value);
        else if (value instanceof List<?>){
            JsonArray array = new JsonArray();
            for (Object o : (List<?>) value)
                array.add(convertObjectToElement(o));
            element = array;
        } else if (value instanceof Map) {
            try {
                JsonObject object = new JsonObject();
                @SuppressWarnings("unchecked")
                Map<String, Object> map = (Map<String, Object>) value;
                for (Map.Entry<String, Object> entry : map.entrySet())
                    object.add(entry.getKey(), convertObjectToElement(entry.getValue()));
                element = object;
            } catch (ClassCastException ex){
                ex.printStackTrace();
            }
        }
        return element;
    }

    /**
     * @param configClass The config class.
     * @return The json file that contains all the settings.
     */
    public static File getConfigFile(Class<?> configClass) {
        String path;
        if (isRegistered(configClass)){
            ConfigClass clazz = configClass.getAnnotation(ConfigClass.class);
            Plugin plugin = LUtils.getPlugin(clazz.parentPlugin());
            if (plugin != null){
                path = "plugins" + File.separator + plugin.getName() + File.separator + clazz.jsonFile();
                return new File(path);
            } else {
                return null;
            }
        }
        return null;
    }

    /**
     * @param configClass The config class.
     * @return Whether or not the json config file exists.
     */
    public static boolean configExists(Class<?> configClass) {
        try {
            return Objects.requireNonNull(getConfigFile(configClass), "The config file does not exist.").exists();
        } catch (NullPointerException ex){
            return false;
        }
    }

    /**
     * @param configClass The config class that you want to be unregistered.
     * @return Whether or not it was successfully unregistered.
     */
    public static boolean unregisterClass(Class<?> configClass) {
        if (!isRegistered(configClass))
            return false;

        configClasses.remove(configClass);

        return true;
    }

    /**
     * @param configClass The config class.
     * @return Whether or not it has been registered.
     */
    public static boolean isRegistered(Class<?> configClass) {
        return configClass.isAnnotationPresent(ConfigClass.class) && configClasses.containsKey(configClass);
    }

    /**
     * @param configClass The class that you want the entry of.
     * @return A map entry containing the class and option fields.
     */
    public static Map.Entry<Class<?>, List<Field>> getEntry(Class<?> configClass) {
        if (!isRegistered(configClass))
            return new AbstractMap.SimpleEntry<>(configClass, Collections.emptyList());
        for (Map.Entry<Class<?>, List<Field>> next : configClasses.entrySet()) {
            if (next.getKey().equals(configClass))
                return next;
        }
        return new AbstractMap.SimpleEntry<>(configClass, Collections.emptyList());
    }

}
