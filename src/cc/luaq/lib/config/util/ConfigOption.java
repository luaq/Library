package cc.luaq.lib.config.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * LLib v2 was created and distributed by Luaq.
 * Please do not redistribute as one's own regardless of the
 * situation, this project (code and all), belongs to Luaq.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ConfigOption {

    /**
     * This could be used for a number of things.
     * @return The display name of the option.
     */
    String displayName() default "";

    /**
     * Do note that periods are ignored as of right now in paths
     * and are not treated like they are in the Bukkit YML paths.
     * @return The option path for in the json config file.
     */
    String optionPath();

}
