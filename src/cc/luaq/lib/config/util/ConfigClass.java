package cc.luaq.lib.config.util;

import org.bukkit.plugin.Plugin;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * LLib v2 was created and distributed by Luaq.
 * Please do not redistribute as one's own regardless of the
 * situation, this project (code and all), belongs to Luaq.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ConfigClass {

    /**
     * @return The json file name.
     */
    String jsonFile() default "config.json";

    /**
     * @return The plugin that this config belongs to.
     */
    Class<? extends Plugin> parentPlugin();

}
