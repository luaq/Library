package cc.luaq.lib;

import cc.luaq.lib.command.LCommandUtils;
import cc.luaq.lib.commands.LLibCommand;
import cc.luaq.lib.config.LConfig;
import cc.luaq.lib.config.util.ConfigClass;
import cc.luaq.lib.gui.events.MenuListener;
import cc.luaq.lib.util.LUtils;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * LLib v2 was created and distributed by Luaq.
 * Please do not redistribute as one's own regardless of the
 * situation, this project (code and all), belongs to Luaq.
 */
public class LLib extends JavaPlugin {

    @Override
    public void onEnable() {
        if (LConfig.registerClass(Config.class)) {
            getLogger().info("Registered config class for " + LConfig.getEntry(Config.class).getKey().getAnnotation(ConfigClass.class).jsonFile());
        } else {
            getLogger().severe("Unable to register config. As a result, plugin shutting down.");
            getPluginLoader().disablePlugin(this);
            return;
        }

        if (LUtils.refreshNMS())
            getLogger().info("Your NMS version was detected: " + LUtils.NMS_VERSION);
        else {
            getLogger().warning("There was no NMS version found for " + LUtils.NMS_VERSION + ". Taking this into consideration, you may have to enable reflection in llib.json.");
        }

        if (Config.USE_REFLECTION)
            getLogger().warning("Your server is currently using reflection for NMS compatibility. Reflection is a slow method of doing things and should only be used if completely necessary. You can always disable this in the llib.json file.");

        registerCommands();
        registerEvents();
    }

    private void registerCommands() {
        LCommandUtils.register(this, "llib", new LLibCommand(), true);
    }

    private void registerEvents(){
        PluginManager pluginManager = Bukkit.getPluginManager();

        pluginManager.registerEvents(new MenuListener(), this);
    }

}
