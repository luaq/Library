# LLib
Library and API plugin for Bukkit development to make everyone's life easier.
## Features
Below are just some of the features that make Bukkit development much easier for developers!
Some of these features include:

 - Json Configs
 - Commands Made Easier

### Json Config
With LLib you can create json configuration files, and it's easy as well!
#### Creating Config Class
To get started, you will need to create a class with the annotation [`@ConfigClass`](https://github.com/Luaqs/Library/blob/master/src/cc/luaq/lib/config/util/ConfigClass.java).

    import cc.luaq.lib.config.util.ConfigClass;
    
    @ConfigClass(jsonFile = "config.json", parentPlugin = YourPluginMainClass.class)
    public class ConfigExample {
		
	}

#### Registering The Config Class
Next thing you're going to want to do, is register the config class with the [LConfig class](https://github.com/Luaqs/Library/blob/master/src/cc/luaq/lib/config/LConfig.java), otherwise saving things, etc. will not work.

    import cc.luaq.lib.config.LConfig;
    
    @Override
    public void onEnable() {
		if (!LConfig.registerClass(ConfigExample.class)) {
			/* do something if it failed to register */
		}
	}

#### Adding Config Options
The config class will not register unless there are some values being associated with it, so let's add some.
To create a config option, you first head to the config class that you want the options to be in, and create a field with the annotation [`@ConfigOption`](https://github.com/Luaqs/Library/blob/master/src/cc/luaq/lib/config/util/ConfigOption.java):

    import cc.luaq.lib.config.util.ConfigClass;
    import cc.luaq.lib.config.util.ConfigOption;
    
    @ConfigClass(jsonFile = "config.json", parentPlugin = YourPluginMainClass.class)
    public class ConfigExample {
		
		/*
		* If you don't plan on modifiying the value with code (ex. ConfigExample.exampleValue = true;), then please declare the field final.
		*/
		@ConfigOption(optionPath = "path_to_value_in_json_file")
		public static boolean exampleValue = false;
		
	}

*Note: It is required that you add the access modifiers: `public` and `static` to any config option in the class.*
#### Saving The Config Values
To save the fields' values in the config class and write them to the json file, you can use the [`writeConfigFile()`](https://github.com/Luaqs/Library/blob/master/src/cc/luaq/lib/config/LConfig.java#L110) method.

    if (!LConfig.writeConfigFile(ConfigExample.class)) {
		/* do something if it failed to save the data */
	}

#### Updating Config Class Values
To update the fields in the config class to what's in the config json file, you can use the [`updateConfigValues()`](https://github.com/Luaqs/Library/blob/master/src/cc/luaq/lib/config/LConfig.java#L55) method.

    if (!LConfig.updateConfigValues(ConfigExample.class)) {
		/* do something if it failed to update the fields */
	}

### Commands Made Easier  
With LLib, creating commands is now easier! Let's demonstrate.
#### Creating a Command
To create a simple command you will start with a fresh class:

    public class CommandExample {
		
	}

To declare this class a command, you should implement, instead of using [CommandExecutor](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/command/CommandExecutor.html), we will use the [LCommand](https://github.com/Luaqs/Library/blob/master/src/cc/luaq/lib/command/LCommand.java) interface like shown in the code below. Along with implementing the interface, you will need to override the [`onCommand()`](https://github.com/Luaqs/Library/blob/master/src/cc/luaq/lib/command/LCommand.java#L13) method; this is where your command code will go.

	import cc.luaq.lib.command.LCommand;
	import cc.luaq.lib.util.LSender;  
	import org.bukkit.command.Command;
	
    public class CommandExample implements LCommand {
		
		@Override
		public void onCommand(LSender sender, Command command, String args[]) {
			/* your command code will go in here */
		}

	}

#### Registering Your Command
Once you've written your command and added it to the plugin.yml file, you will want to register it with the [`LCommandUtils.register()`](https://github.com/Luaqs/Library/blob/master/src/cc/luaq/lib/command/LCommandUtils.java#L29) method as demonstrated below.

	import cc.luaq.lib.command.LCommandUtils;
	
    @Override
    public void onEnable() {
		LCommandUtils.register(this, "examplecommand", new CommandExample(), false);
	}

#### Why it's Easier
You'll notice that the `onCommand()` method has a `void` and not a `boolean` return value, this is just one of the many things that is easier with commands in LLib.

**Blocking Console Access**
A lot of people dislike the idea of console having access to player commands, and LLib has made this easy. To simply know if the command sender is a player, you can do the following:

    @Override
	public void onCommand(LSender sender, Command command, String args[]) {
		if (!sender.isPlayer()){
			sender.send("&cOnly players can use this command.");
			return;
		}
	}
*Note: This can be made even simpler by disabling console when registering the command.*

**Sending Messages**
If you're getting tired of writing out `player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cSome random message."))`, then you're going to love the simplicity of this one. With LLib, all you need to do to send a nice pretty message to someone is the following:

    @Override
	public void onCommand(LSender sender, Command command, String args[]) {
		sender.send("&cSome random message.");
	}

**Sending Usage Message**
To send the usage *(usage is specified in the plugin.yml)* of a command in LLib, you can simply do the following:

	@Override
	public void onCommand(LSender sender, Command command, String args[]) {
		sender.sendUsage(command);
	}

**Exception Handling**
Instead of that plain old, "Internal Error" message, it provides you with the following information:
- what caused the error *(ex. java.lang.NullPointerException)* 
- the class, method, and line number that the error occurred on 

*Note: this also keeps the console clean looking as well due to the custom error message.*

### Things are still being developed and documented, please be patient!